function initDatabase() {
    try {
        if (!window.openDatabase) {
            alert('Databases are not supported in this browser.');
        } else {
            var shortName = 'html5nuclear_cookies';
            var version = '1.0';
            var displayName = 'html5nuclear_cookies';
            var maxSize = 100000; //  bytes
            DEMODB = openDatabase(shortName, version, displayName, maxSize);
            createTables();
        }
    } catch(e) {
        if (e == 2) {
            // Version number mismatch.
            console.log("Invalid database version.");
        } else {
            console.log("Unknown error "+e+".");
        }
        return;
    }
}

function insertSession(sessionId) {
    DEMODB.transaction(
        function (transaction) {
            transaction.executeSql( "REPLACE INSERT INTO html5nuclear_cookies (session) VALUES('" + sessionId+  "')", [],
            dataSelectHandler, errorHandler);
        }
    );    
}

function getSession(clientId) {
    DEMODB.transaction(
        function (transaction) {
            transaction.executeSql("SELECT * FROM html5nuclear_cookies WHERE ;", [],
            dataSelectHandler, errorHandler);
        }
    );
}

function createTables(){
    DEMODB.transaction(
        function (transaction) {
        	transaction.executeSql('CREATE TABLE IF NOT EXISTS page_settings(sessionId varchar(64) NOT NULL PRIMARY KEY, clientId INTEGER);', [], nullDataHandler, errorHandler);
        }
    );	
}