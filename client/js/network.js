//////////////////////////////////////////////////////////////////////////////////
// Network code
////////////////////////////////////////////////////////////////////////////////
var myGameId = 0; // Global variable set when server send rcv-joined. 
var myPlayerId = 0; // Global variable set when joining a lobby
var myAction;	    // What i do this turn


/*
struct packet_action {
   int turn;
   int player;	//	ignored when client sends
   int action;
   int target_player;
   int target_city;
};*/

window.onload = function () {
    connect();
    jQuery('#newGameButton').click(createNewGame);
    jQuery('#debugButton').click(sendDebugMessage);
    jQuery('#startGameButton').click(startGame);
}

jQuery(document).ready( function(){

});

function checkMySession(){
    if( localStorage.getItem('sessionId') !== null  ) {
        // Here we should probably request my state from server.
        var packet = new Array();
        packet[0] = "session_status";
        packet[1] = localStorage.getItem('sessionId');
        console.log(packet);
        send( array2json( packet ) );    
    }
}

/**
 * Draws the list of games
 */
function drawGameList( message ) {
    jQuery("#gameLists").empty();
    // HERE IS A BUG THAT MAKES JQUERY DIEE!!!!!
    // THIS makes IT IMPOSSIBLE TO RUN CODE AFTER!!!!!
    jQuery.each( message.data, function( index ) {
	jQuery("#gameLists").append( "<li> <a href='' onclick='joinGame("+index+");return false;'>" + message.data[index]['gamename'] + " - " + message.data[index]['playerCount'] + " Players - State: "+message.data[index]['state']+"</a></li>");
    });
    
}

/**
 * Sends startgame packet.
 */
function startGame() {
    var packet = new Array();
    packet[0] = "start";
    packet[1] = myGameId;
    send( array2json ( packet ) );
}

/**
 * Sends a request to start a new game
 */
function createNewGame() {
    var packet = new Array();
    packet[0] = 'newgame';
    packet[1] = jQuery('#newPlayerName').val();
    send( array2json( packet ) );
}

/**
 * Handles response for joining a game
 */
function joinedGameResponse( message ) {
    myGameId = message.data.gameId;
    myPlayerId = message.data.playerId;
    if(!message.data.sessionId) {
        console.log("SERIOUS ERROR NO SEESSION ID");
    }
    
    var packet = new Array();
    packet[0] = "status";
    packet[1] = myGameId;
    send( array2json( packet ) );    
    jQuery('#startGameButton').show();
}

/**
 * Event used when clicking on join game button. Sends request for joining a game
 */
function joinGame( gameId ) {
    jQuery('#newGameArea').hide();
    jQuery('#gameLobbyArea').show();
    // Send we want to join to server
    var packet = new Array();
    packet[0] = "join";
    packet[1] = gameId;
    packet[2] = jQuery( '#newPlayerName' ).val();
    send( array2json( packet ) );
}

/**
 * Displays all players currently in game, called when we recieve rcv-status
 */
function drawGameLobby( message ) {
    jQuery('#newGameArea').hide();
    jQuery('#gameLobbyArea').show();
    
    jQuery("#playersList").empty();
    jQuery.each( message.data.players, function( index ) {
	jQuery("#playersList").append( "<li>" + message.data.players[index]['playerName'] + "</li>");
    });
}

/**
 * Connect to server
 */
function connect() {
    log('Connecting...');
    Server = new FancyWebSocket('ws://127.0.0.1:12345');

    $('#message').keypress(function(e) {
	if ( e.keyCode == 13 && this.value ) {
	    log( 'You: ' + this.value );
	    send( this.value );

	    $(this).val('');
	}
    });

    //Let the user know we're connected
    Server.bind('open', function() {
	log( "Connected." );
    });

    //OH NOES! Disconnection occurred.
    Server.bind('close', function( data ) {
	log( "Disconnected." );
    });

    //Log any messages sent from server
    Server.bind('message', function( message ) {
	log( "Recieved: "+message );
	recieve( message );
    });

    Server.connect();
    

}

function setSession( sessionId ) {
    localStorage.sessionId = sessionId;
}

function recieve( message ) {
    
    message = jQuery.parseJSON( message );
        
    switch( message.type ) {
	case "rcv-list" :
            console.log("SCV1");
	    drawGameList(message);
            console.log("SCV2");
            checkMySession();
	    break;
	case "rcv-joined":
	    joinedGameResponse(message);
	    break;
	case "rcv-status" :
	    drawGameLobby( message );
	    break;
        case "rcv-session" :
            setSession( message.data );
        break;
	case "rcv-turn":
	    onTurn( message );
	    break;
    }
}

function log( text ) {
    $log = $('#log');
    //Add text to log
    $log.append(($log.val()?"\n":'')+text);
    //Autoscroll
    $log[0].scrollTop = $log[0].scrollHeight - $log[0].clientHeight;
}

function send( text ) {
    Server.send( 'message', text );
}

function sendDebugMessage() {
    log("Sending debug msg: "+ jQuery( '#debugMessageType' ).val() + ","+ jQuery( '#debugMessageData' ).val() );

    var packet = new Array();
    packet[0] = jQuery( '#debugMessageType' ).val();
    packet[1] = jQuery( '#debugMessageData' ).val();
    
    send( array2json( packet ) );
}

/**
 * MISC
 */
function array2json(arr) {
    var parts = [];
    var is_list = (Object.prototype.toString.apply(arr) === '[object Array]');

    for(var key in arr) {
    	var value = arr[key];
        if(typeof value == "object") { //Custom handling for arrays
            if(is_list) parts.push(array2json(value)); /* :RECURSION: */
            else parts[key] = array2json(value); /* :RECURSION: */
        } else {
            var str = "";
            if(!is_list) str = '"' + key + '":';

            //Custom handling for multiple data types
            if(typeof value == "number") str += value; //Numbers
            else if(value === false) str += 'false'; //The booleans
            else if(value === true) str += 'true';
            else str += '"' + value + '"'; //All other things
            // :TODO: Is there any more datatype we should be in the lookout for? (Functions?)

            parts.push(str);
        }
    }
    var json = parts.join(",");
    
    if(is_list) return '[' + json + ']';//Return numerical JSON
    return '{' + json + '}';//Return associative JSON
}

