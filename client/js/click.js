function click_ml_10() {
  $('#sfx_fanfare').trigger("play");
  myAction = "ML_10";
}

function click_ml_20() {
  $('#sfx_fanfare').trigger("play");
  myAction = "ML_20";
}

function click_ml_50() {
  $('#sfx_fanfare').trigger("play");
  myAction = "ML_50";
}

function click_ml_100() {
  $('#sfx_fanfare').trigger("play");
  myAction = "ML_100";
}

function click_wh_10() {
  $('#sfx_fanfare').trigger("play");
  myAction = "WH_10";
}

function click_wh_20() {
  $('#sfx_fanfare').trigger("play");
  myAction = "WH_20";
}

function click_wh_50() {
  $('#sfx_fanfare').trigger("play");
  myAction = "WH_50";
}

function click_wh_100() {
  $('#sfx_fanfare').trigger("play");
  myAction = "WH_100";
}


function click_bmbr_np1() {
  $('#sfx_engine').trigger("play");
  myAction = "BMBR_NP1";
}

function click_bmbr_gr2() {
  $('#sfx_engine').trigger("play");
  myAction = "BMBR_GR2";
}

/**
 * Defence lnds.
 */
function click_dfnce_lnds() {
  $('#sfx_dish').trigger("play");
  myAction = "DFNCE_LNDS";
}

/**
 * Defence mage
 */
function click_dfnce_mega() {
  $('#sfx_dish').trigger("play");
  myAction = "DFNCE_MEGA";
}

/**
 * Send propaganda
 */
function click_propaganda() {
  $('#sfx_radio').trigger("play");
  myAction = "PROPAGANDA";
}

/**
 * Build more weapons!
 */
function click_factory() {
  $('#sfx_build').trigger("play");
  myAction = "FACTORY";
}

/**
 * Clicked on player space 0.
 */
function click_player_space(country_id) {
  player = get_player_by_country(country_id);
  if (player != undefined) {

    layer_screen.markRectDamaged(new CanvasLayers.Rectangle(0, 0, 640, 480));
    if (country_id_focused == country_id){
      // 4 always client!
      country_id_focused = 4;
    }
    else {
      country_id_focused = country_id;
    }
    container.redraw();
  }
}

/**
 * Ready to play the move.
 */
function click_ready() {
  doTurn();
}


/**
 * Show target when clicking on a city.
 */
function click_city(id) {
  // Dont allow targeting yourself.
  if (country_id_focused != 4) {
    layer_screen.markRectDamaged(new CanvasLayers.Rectangle(0, 0, 640, 480));
    city_id_focused = id;
    player = get_player_by_country(country_id_focused);
    player_id_target = player.playerId;
    container.redraw();
  }
}