/* Globals */
var firstTurn = true; // if set to true, dont initialize the drawing area again. Or ?
var players = new Array();

var canvas;
var context;
var layer_terminal;
var layer_screen;
var leaders_sprite_interval;
var country_id_focused = 4;
var container;

var font_settings = "15px bold arial";
var font_color = "yellow";
var city_id_focused = null;

var waiting_for_players = false;

var player_id_target = 4;

/**
 * Current player always need to be country 4 on client so this function will set correct country in player_country.
 */
function setup_player_countries(players) {
  counter = 0;
  for (player_id in players) {
    if (player_id == myPlayerId) {
      players[player_id].country_id = 4
    }
    else {
      players[player_id].country_id = counter;
      counter++;
    }
  }
}

/**
 * Helper function, get player by country_id
 */
function get_player_by_country(country_id) {
  for (player_id in players) {
    if (players[player_id].country_id == country_id) {
      return players[player_id];
    }
  }
  return null;
}

/**
 * Called when we recieve a "turn" response from server.
 */
function onTurn( message ) {
    
    players = message.data;
    if(firstTurn) {
	// If this is the first turn then draw game board.
        setup_player_countries(players);
	initGameArea();
	firstTurn = false;
        return;
    }
    waiting_for_players = false;
    play_scene();
}

/**
 * Send action to server
 */
function doTurn() {
  waiting_for_players = true;
  if( ( city_id_focused == undefined || city_id_focused == 0 ) && 
      ( myAction == 'PROPAGANDA' || myAction == 'WH_100' || 
        myAction == 'WH_50' || myAction == 'WH_20' || myAction == 'WH_10') ) {
        alert('NEED TO SELECT TARGET OMG NOOB!');
  }

  var packet = new Array();
  packet['type'] = 'action';
  packet[1] = myGameId;
  packet[2] = myPlayerId;
  packet[3] = myAction;
  packet[4] = country_id_focused;
  packet[5] = city_id_focused;
  send( array2json( packet ) );
  myAction = 0;
  city_id_focused = 0;
  show_wait();  
}

/**
 * Play the scene for the player!
 */
function play_scene() {
  layer_terminal.hide();
  container.redraw();
};

/**
 * Init game when start from lobby!
 */
function initGameArea() {
    jQuery('#gameLobbyArea').hide();
    jQuery('#newGameArea').hide();
    jQuery('#gameListArea').hide();
    jQuery('#playerNameArea').hide();
    init_layers();
    $('#game_area').click(terminal_click_handler);
}

/**
 * Terminal clicking handler.
 */
function terminal_click_handler(e) {
    
  var x = e.pageX - this.offsetLeft;
  var y = e.pageY - this.offsetTop;
  
  command = '';
  param = '';
  
  for(i = 0; i < terminal_coordinates.length; i++) {
    if ((x > terminal_coordinates[i].x1 && x < terminal_coordinates[i].x2) && (y > terminal_coordinates[i].y1 && y < terminal_coordinates[i].y2)) {
      command = terminal_areas[terminal_coordinates[i].id].function_name;
      param = terminal_areas[terminal_coordinates[i].id].country_id;
    }
  }
  

  for (i = 0; i < city_coords[country_id_focused].length; i++) {
      if((x > city_coords[country_id_focused][i].x && x < city_coords[country_id_focused][i].x + 32) && (y > city_coords[country_id_focused][i].y && y < city_coords[country_id_focused][i].y + 32)) {
        command = 'click_city'
        param = i;
      }
  }

  if(command) {
    window[command](param);
  }
}

/**
 * Initiate all canvas layers.
 */
function init_layers() {    
  
  canvas = document.getElementById("game_area");
  container = new CanvasLayers.Container(canvas, true);
  
  container.onRender = render_foreground;
  
  layer_scene = new CanvasLayers.Layer(0, 0, 640, 480);
  container.getChildren().add(layer_scene);
  layer_scene.onRender = render_scene;

  layer_terminal = new CanvasLayers.Layer(0, 0, 640, 480);
  container.getChildren().add(layer_terminal);
  layer_terminal.onRender = render_terminal;
  
  layer_screen = new CanvasLayers.Layer(0, 0, 640, 480);
  layer_terminal.getChildren().add(layer_screen);
  layer_screen.onRender = render_screen;
  
  
  container.redraw();
}

/**
 * Render the scene layer.
 */
function render_scene(layer, rect, context) {
  var imageObj = new Image();
  imageObj.onload = function() {
    context.drawImage(imageObj, 0, 0, 640, 480);
  };
  // Does not let me use the same image twice!
  imageObj.src = "gfx/world2.bmp";
}

/**
 * Render the terminal layer.
 */
function render_terminal(layer, rect, context) {
  var imageObj = new Image();
 
  imageObj.onload = function() {
    canvas.width = imageObj.width;
    canvas.height = imageObj.height;
    context.drawImage(imageObj, 0, 0);
    
    if (waiting_for_players) {
      context.font = font_settings;
      context.fillStyle = 'black';
      context.fillText('Waiting for other players!', 160, 340);
    }
    
  };
  imageObj.src = "gfx/terminal.bmp";
  
  /* initiate leader gfx */
  var leaders_gfx = new Image();
  leaders_gfx.onload = function() {
    context.font = font_settings;
    context.fillStyle = font_color;
    console.log("players: "+players);
    console.log(myPlayerId);
    for (player_id in players) {
      if (player_id != myPlayerId) {
        sprite_name = leader_sprite_name(players[player_id]);
        leader_sprite = leader_sprites.getOffset(sprite_name);
        context.clearRect(country_leader_coords[players[player_id].country_id].leader.x, country_leader_coords[players[player_id].country_id].leader.y, 64, 64);
        context.drawImage(leaders_gfx, leader_sprite.x, leader_sprite.y, 32, 32, country_leader_coords[players[player_id].country_id].leader.x, country_leader_coords[players[player_id].country_id].leader.y, 64, 64);
        context.fillText(players[player_id].playerName, country_leader_coords[players[player_id].country_id].name.x, country_leader_coords[players[player_id].country_id].name.y);
      }
      else {
        enable_leds(players[player_id].inventory, context);
      }
    }
  };
  
  leaders_gfx.src = 'gfx/leaders.bmp';
}

/**
 * Callback for the background layer... what to put here?
 */
function render_foreground() {
// What to do here?  function is needed anyhow!
}




function draw_city(city_id, context) {
  var city_gfx = new Image();
  city_gfx.onload = function() {

    city_sprite = city_sprites.getOffset(city_sprite_name(player.cities[city_id]));
    context.drawImage(city_gfx, city_sprite.x, city_sprite.y, 16, 16, city_coords[country_id_focused][city_id].x, city_coords[country_id_focused][city_id].y, 32, 32);

    context.font = font_settings;
    context.fillStyle = font_color;
    context.fillText(player.cities[city_id], city_coords[country_id_focused][city_id].x + 10, city_coords[country_id_focused][city_id].y + 45);
  };

  city_gfx.src = "gfx/cities.png";
}

/**
 * Render layer for the middle screen inside terminal.
 */
function render_screen(layer, rect, context) {
  var imageObj = new Image();
  imageObj.onload = function() {
    context.drawImage(imageObj, country_coords[country_id_focused].clip_start_x, country_coords[country_id_focused].clip_start_y, 133, 81, 200, 120, 247, 162);
  };
  imageObj.src = "gfx/world.bmp";
  
  player = get_player_by_country(country_id_focused);
  for (i = 0; i < 5; i++) {
    draw_city(i, context);
  }
  
  
  if (city_id_focused != null) {
    if (player_id_target != 4 && player_id_target == player.playerId) {
      var target = new Image();
      target.onload = function() {
        context.drawImage(target, 0, 0, 16, 16, city_coords[country_id_focused][city_id_focused].x, city_coords[country_id_focused][city_id_focused].y, 32, 32);
      }
      target.src = "gfx/target.png";  
    }
  }
  
}


/**
 * Determine the leader sprite name based on total population.
 */

function leader_sprite_name(player) {
  sprite_name = leaders[player.leader].id + '_'; 
  total_population = 0;
  for (var key in player.cities) {
    total_population += player.cities[key];
  }
  
  
  // Put in constants?
  if (total_population > total_population_steps[0]) {
    sprite_name += '1';
  }
  else if (total_population > total_population_steps[1] && total_population <= total_population_steps[0]) {
    sprite_name += '2';
  }
  else if (total_population > total_population_steps[2] && total_population <= total_population_steps[1]) {
    sprite_name += '3';
  }
  else if (total_population > total_population_steps[3] && total_population <= total_population_steps[2]) {
    sprite_name += '4';
  }
  else if (total_population > 0 && total_population <= total_population_steps[3]) {
    sprite_name += '5';
  }
  else if (total_population == 0 ) {
    sprite_name = null;
  }
  
  return sprite_name;
}

/**
 * Determine the city sprite based on city poulation.
 */
function city_sprite_name(population) {
  sprite_name = '';
  // Put in constants.
  if (population == 0) {
    sprite_name = 'city_1';
  }
  else if(population > 0 && population <= city_population_steps[3]) {
    sprite_name = 'city_2';
  }
  else if(population > city_population_steps[3] && population <= city_population_steps[2]) {
    sprite_name = 'city_3';
  }
  else if(population > city_population_steps[2] && population <= city_population_steps[1]) {
    sprite_name = 'city_4';
  }
  else if(population > city_population_steps[1] && population <= city_population_steps[0]) {
    sprite_name = 'city_5';
  }
  else if(population > city_population_steps[0]) {
    sprite_name = 'city_6';
  }
  
  return sprite_name;
}

/**
 * Enable leds based on inventory.
 */
function enable_leds(inventory, context) {

  for (var id in inventory) {
    if (inventory[id] > 0) {
      lit_led(id, context);
    }
  }
}

/**
 * Lite one led.
 */
function lit_led(id, context) {
  var led_gfx = new Image();
  led_gfx.onload = function() {
    context.drawImage(led_gfx, terminal_led_position[id].x, terminal_led_position[id].y, 10, 10);
  }
  led_gfx.src = 'gfx/led.bmp';
}

/**
 * Tell people to wait for other players.
 */
function show_wait() {
  layer_terminal.markRectDamaged(new CanvasLayers.Rectangle(0, 0, 640, 480));
  container.redraw();
}







/*
 * Network code problems:
 * Player show get their own inventory only!
 */

// NO leader animation needed but save for 
// var leaders_gfx = new Image();
//  leaders_gfx.onload = function() {
//    leaders_sprite_interval = setInterval(function(){
//      if(players[1] != undefined) {
//	command = 'anim_' + leaders[players[1].leader].id;
//	window[command].animate(timer.getSeconds());
//	frame = window[command].getSprite();
//	context.clearRect(25, 10, 64, 64);
//	context.drawImage(leaders_gfx, frame.x, frame.y, 32, 32, 25, 10, 64, 64);
//      }
//      
//      if(players[2] != undefined) {      
//	command = 'anim_' + leaders[players[2].leader].id;
//	window[command].animate(timer.getSeconds());
//	frame = window[command].getSprite();
//	context.clearRect(535, 10, 64, 64);
//	context.drawImage(leaders_gfx, frame.x, frame.y, 32, 32, 535, 10, 64, 64);
//      }
//      
//      if(players[3] != undefined) {      
//	command = 'anim_' + leaders[players[3].leader].id;
//	window[command].animate(timer.getSeconds());
//	frame = window[command].getSprite();
//	context.clearRect(25, 385, 64, 64);
//	context.drawImage(leaders_gfx, frame.x, frame.y, 32, 32, 25, 385, 64, 64);
//      }
//      if(players[4] != undefined) {      
//	command = 'anim_' + leaders[players[4].leader].id;
//	window[command].animate(timer.getSeconds());
//	frame = window[command].getSprite();
//	context.clearRect(535, 385, 64, 64);
//	context.drawImage(leaders_gfx, frame.x, frame.y, 32, 32, 535, 385, 64, 64);
//      }
//      timer.tick();
//    }, 5);
//    
//
//    
//    if(players[1] != undefined) {
//	context.font = font_settings;
//	context.fillStyle = font_color;
//	context.fillText(players[1].playerName, 30, 100);
//	enable_leds(players[1].inventory, context);
//    }
//    if(players[2] != undefined) {    
//	context.font = font_settings;
//	context.fillStyle = font_color;
//	context.fillText(players[2].playerName, 540, 100);
//	enable_leds(players[2].inventory, context);
//    }
//    
//    if(players[3] != undefined) {
//	context.font = font_settings;
//	context.fillStyle = font_color;
//	context.fillText(players[3].playerName, 30, 470);
//	enable_leds(players[3].inventory, context);
//    }
//    
//    if(players[4] != undefined) {
//	context.font = font_settings;
//	context.fillStyle = font_color;
//	context.fillText(players[4].playerName, 540, 470);
//	enable_leds(players[4].inventory, context);
//    }
//  };
//  
//
//  
//  leaders_gfx.src = 'gfx/leaders.bmp';